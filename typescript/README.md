# Poker (http://dojopuzzles.com/problemas/exibe/poker/)

Implementação usando typescript

## Como construir e rodar?

Para construir é necessário que tenha o nodejs instalado e typescript.

1- Download node: https://nodejs.org/en/download/

2- Download typescript:

```
npm install -g typescript
```

3- Compilar/Construir:

```
tsc poker.ts
```

4- Executar/Rodar:

```
node poker.js
```