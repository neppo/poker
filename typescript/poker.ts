interface Card
{
    suit: string;
    rank: number;
    limit?: number;
}

interface DeckOptions
{
    extend: Card[];
    suits: string[];
    ranks: number[];
    multiply: number;
}

class Deck
{
    private _opt: DeckOptions;

    cards: Card[];

    constructor (opt: Object = {})
    {
        this._opt = {
            extend: opt['extend'] || [],
            suits: opt['suits'] || ['spades', 'hearts', 'diamonds', 'clubs'],
            ranks: opt['ranks'] || [14/*ace*/, 13/*king*/, 12/*queen*/, 11/*jack*/, 10, 9, 8, 7, 6, 5, 4, 3, 2],
            multiply: opt['multiply'] || 1
        };

        if (this._opt.multiply < 1)
            this._opt.multiply = 1;

        this.shuffle();
    }

    shuffle ()
    {
        this.cards = [];

        for (let i = 0; i < this._opt.multiply; i++) {
            for (let suit of this._opt.suits) {
                for (let rank of this._opt.ranks) {
                    this.inlay({ suit: suit, rank: rank });
                }
            }
            for (let card of this._opt.extend) {
                if (!card.limit || i < card.limit)
                    this.inlay({ suit: card.suit, rank: card.rank });
            }
        }
    }

    inlay (card: Card): boolean
    {
        if (card && card.suit && card.rank) {
            this.cards.push(card);
            return true;
        }
        else
            return false;
    }

    count (): number
    {
        return this.cards.length;
    }

    draw (): Card
    {
        let count = this.count();
        if (count > 0)
            return this.cards.splice(Math.floor(Math.random() * count), 1)[0] || null;
        else
            return null;
    }

    
}

enum TypeValues {
    CartaAlta = 1,//: A carta de maior valor.
    Par = 2,//Duas cartas do mesmo valor. 
    DoisPares = 3,// Dois pares diferentes.
    Trinca = 4,//: Três cartas do mesmo valor e duas de valores diferentes.
    Straight = 5,// (seqüência): Todas as carta com valores consecutivos.
    Flush = 6,//: Todas as cartas do mesmo naipe.
    FullHouse = 7,//: Um trinca e um par.
    Quadra = 8,//: Quatro cartas do mesmo valor.
    StraightFlush = 9,//: Todas as cartas são consecutivas e do mesmo naipe.
    RoyalFlush = 10//: A seqüência 10, Valete, Dama, Rei, Ás, do mesmo naipe.
    //As cartas são, em ordem crescente de valor: 2, 3, 4, 5, 6, 7, 8, 9, 10, Valete, Dama, Rei, Ás.
    //Os naipes são: Ouro (D), Copa (H), Espadas (S), Paus (C)
}

interface HandTestable {
    test(): boolean;
    value(): TypeValues;
}

class BaseClass {
    private hand: Card[];
    private originalHand: Card[];
    constructor (hand : Card[]){
        this.hand = this.sortHand(hand);
    }

    public getHand() : Card[]{
        return this.hand;
    }

    private sortHand(hand : Card[]) : Card[]{
        hand = hand.sort((obj1, obj2) => {
            if (obj1.rank > obj2.rank) {
                return 1;
            }
        
            if (obj1.rank < obj2.rank) {
                return -1;
            }
        
            return 0;
        });
        this.originalHand = hand.map(obj => ({...obj}));
        return hand;
    }

    public sameValuesRepeat(repeat : number, remove = false) : boolean {
        let counter = 0;
        let lastCard : Card = null;
        let firsIndexToRemove : number;

        let rankToObserve : number;

        for(let i = 0; i < this.hand.length; i++){
            let card = this.hand[i];
            if(lastCard != null && 
                (!rankToObserve || card.rank == rankToObserve) && 
                card.rank==lastCard.rank){
                counter++;
                if(!rankToObserve){
                    rankToObserve = card.rank;
                }
                if(remove && !firsIndexToRemove){
                    firsIndexToRemove = i;
                }
            }
            lastCard = card;
        }

        let repeated = repeat - 1 == counter;
        if(repeated && remove){
            this.hand.splice(firsIndexToRemove, repeat-1);
        }

        return repeated;
    }

    public sameSuit() : boolean {
        let firstCard = this.hand[0];
        const sameSuit = this.hand.every((card) => {
            return card.suit == firstCard.suit;
        });

        return sameSuit;
    }

    private sequential(counter : boolean) : any {
        let lastCard : Card = null;
        let count = 0;

        for(let i = 0; i < this.hand.length; i++){
            let card = this.hand[i];
            if(lastCard != null && (card.rank == lastCard.rank || (card.rank - lastCard.rank != 1))){
                if(counter){
                    count++;
                }else{
                    return false;
                }
            }
            lastCard = card;
        }
        
        return counter?count:true;
    }

    public isSequential() : boolean {
        return this.sequential(false);
    }

    public countSequential() : number {
        return this.sequential(true);
    }

    public getMaxValue() : number {
        return this.originalHand[this.originalHand.length-1].rank;
    }
}

class RoyalFlush extends BaseClass implements HandTestable {
    public test(): boolean {       
        const hand = super.getHand();
        return super.sameSuit() && 
            (hand[0].rank==10 && 
            hand[1].rank==11 && 
            hand[2].rank==12 && 
            hand[3].rank==13 && 
            hand[4].rank==14);
    }

    public value() : TypeValues{
        return TypeValues.RoyalFlush;
    }
}

class StraightFlush extends BaseClass implements HandTestable {
    public test(): boolean {       
        return super.sameSuit() && super.isSequential();
    }

    public value() : TypeValues{
        return TypeValues.StraightFlush;
    }
}

class Quadra extends BaseClass implements HandTestable {
    public test(): boolean {       
        return super.sameValuesRepeat(4, true);
    }

    public value() : TypeValues{
        return TypeValues.Quadra;
    }
}

class FullHouse extends BaseClass implements HandTestable {

    public test(): boolean {
        let par = new Par(super.getHand().map(obj => ({...obj})));
        let trinca = new Trinca(super.getHand().map(obj => ({...obj})));

        return par.test() && trinca.test();
    }

    public value() : TypeValues{
        return TypeValues.FullHouse;
    }
}

class Flush extends BaseClass implements HandTestable {
    public test(): boolean {       
        return super.sameSuit();
    }

    public value() : TypeValues{
        return TypeValues.Flush;
    }
}

class Straight extends BaseClass implements HandTestable {
    public test(): boolean {       
        return super.isSequential();
    }

    public value() : TypeValues{
        return TypeValues.Straight;
    }
}

class Trinca extends BaseClass implements HandTestable {
    public test(): boolean {       
        return super.sameValuesRepeat(3, true);
    }

    public value() : TypeValues{
        return TypeValues.Trinca;
    }
}

class DoisPares extends BaseClass implements HandTestable {
    public test(): boolean {
        let hand = super.getHand().map(obj => ({...obj}));
        return new Par(hand).test() && new Par(hand).test();
    }

    public value() : TypeValues{
        return TypeValues.DoisPares;
    }
}

class Par extends BaseClass implements HandTestable {
    public test(): boolean {
        return super.sameValuesRepeat(2, true);
    }

    public value() : TypeValues{
        return TypeValues.Par;
    }
}

class LogicalHandExecutor {

    private executor : HandTestable; 
    constructor (executor : HandTestable)
    {
        this.executor = executor;
    }

    public getValue(): number{
        const test = this.executor.test();
        console.log(TypeValues[this.executor.value()] + " ? "+ test);
        return test ? this.executor.value() : 0;
    }
}

class TestHand {

    public static getPoints(hand: Card[]) : number {
        let points = 0;
        const royalFlush = new LogicalHandExecutor(new RoyalFlush(hand)).getValue()
        points += royalFlush;        
        if(royalFlush)
            return points;

        const straightFlush = new LogicalHandExecutor(new StraightFlush(hand)).getValue();
        points += straightFlush;
        if(straightFlush)
            return points;

        points += new LogicalHandExecutor(new Flush(hand)).getValue();
        points += new LogicalHandExecutor(new Quadra(hand)).getValue();

        const fullHouse = new LogicalHandExecutor(new FullHouse(hand)).getValue();
        points += fullHouse;
        if(fullHouse)
            return points;

        const straight = new LogicalHandExecutor(new Straight(hand)).getValue();
        points += straight;
        
        
        points += new LogicalHandExecutor(new Trinca(hand)).getValue();
        const doisPares = new LogicalHandExecutor(new DoisPares(hand)).getValue()
        points += doisPares;
        if(!doisPares){
            points += new LogicalHandExecutor(new Par(hand)).getValue();
        }
        return points;
    }
}

class Main {


    public run(): void {
        const deck = new Deck();
        console.log("Deck count:"+deck.count());

        var handOne:Card[] = [deck.draw(), deck.draw(), deck.draw(), deck.draw(), deck.draw()];
        var handTwo:Card[] = [deck.draw(), deck.draw(), deck.draw(), deck.draw(), deck.draw()];
        console.log("Deck count after draw hands:"+deck.count());
        console.log(deck.count());

        console.log("------------------------------------")
        console.log(">>>>>>>>>>>> Hand one")
        console.log(handOne);
        console.log("------------------------------------")
        console.log(">>>>>>>>>>>> Hand Two")
        console.log(handTwo);

        console.log("------------------------------------")
        console.log(">>>>>>>>>>>> Hand one")
        const handOnePoints = TestHand.getPoints(handOne);
        console.log("pointsOf HandOne: "+handOnePoints);
        console.log("------------------------------------")
        console.log(">>>>>>>>>>>> Hand two")
        const handTwoPoints = TestHand.getPoints(handTwo);
        console.log("pointsOf HandTwo: "+handTwoPoints);
        console.log("------------------------------------")
        
        const equals = handOnePoints == handTwoPoints;
        if(equals){
            const handOneGreater = new BaseClass(handOne).getMaxValue() > new BaseClass(handTwo).getMaxValue();
            this.log(handOneGreater);
        }else{
            const handOneGreater = handOnePoints > handTwoPoints;
            this.log(handOneGreater);
        }
    }

    public log(handOneGreater : boolean): void{
        if(handOneGreater){
            console.log("hand one wins!");
        }else{
            console.log("hand two wins!");
        }
    }

}




 
new Main().run();